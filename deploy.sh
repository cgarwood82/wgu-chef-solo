#!/bin/bash

# First provision a server and save the public IP somewhere 
knife linode server create --linode-node-name $1 | tee /tmp/$1

# Turn the IP into a variable
IP=$(grep Public /tmp/$1|sed s/^.*:\ //)

#Copy a local key to the new server
ssh-copy-id root@$IP

# Install chef-client
bundle exec knife solo prepare root@$IP 2> /dev/null

# Configure the server
bundle exec knife solo cook root@$IP nodes/wgunode.json 2> /dev/null 
