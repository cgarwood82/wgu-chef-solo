# WGU Chef Solo Project
This project functions as a working example of my project to deploy a jekyll blog to a Linode server on CentOS 7. Most of the cookbooks used are from the chef supermarket with the exception of the el_jeykll, short for Enterprise Linux Jekyll. I hacked this cookbook out over the period of a week. It works, but I'm sure it could have been done better in a few places. 

This readme servers as a general overview of the this working environment as well as what needs to be in place for someone else to test, implement, and use for themselves. 

Chef solo is new to me so I did a fair amount of learning and growing in doing this project. I hope it is useful to anyone else who comes across it.

## Step 0. Install chef-dk for your system.
This can be installed a number of ways, but the source of the installation can be found at https://downloads.chef.io/chefdk

## Step 1. Clone this repository and change to that directory
```
git clone git@bitbucket.org:transmutated/wgu-chef-solo.git && cd wgu-chef-solo
```

## Step 2. Install Gems
```
bundle install
```

## Step 3. Rename and edit the .chef/knife.rb.backup file to .chef/knife.rb
Please see the comments in the file to fillout the attributes for Linode provisioning

## Step 4. Execute librarian-chef to install all of the cookbooks
```
librarian-chef install
```

## Step 5. Edit variables in the nodes/wgunode.json file
Currently, the attributes in this file make sense for my project. Please modify each attribute accordingly.

## Step 6. Provision your new blog with the following:
NOTE: This step requires you have an SSH key pair generated as it will use key based auth going forward. If youwould rather use a password, simply remove the line in deploy.sh that executes ssh-copy-id. 
```
./deploy.sh NAME_OF_YOUR_SERVER
```

## Step 7. Profit

#To do:
* Use attributes for cron job. Currently statically linked to my web deploy root. 
* Create a system user to remotely manage node with instead of root.
* Disable root login with chef cookbook AFTER system user is created. 
